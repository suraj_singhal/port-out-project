package com.mit.lkm.business.bean;

import java.sql.Timestamp;
import java.util.Date;

public class Customer {

	private String custId;

	private Date creationDate;
	
	private Timestamp updateDate;

	private String lastName;

	private String firstName;

	private Date custDOB;

	private String custStatus;

	private Date contractStart;

	private Integer custBalance;

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Customer(Customer customer) {

		super();
		this.custId = customer.getCustId();
		this.creationDate = customer.getCreationDate();
		this.updateDate = customer.getUpdateDate();
		this.lastName = customer.getLastName();
		this.firstName = customer.getFirstName();
		this.custDOB = customer.getCustDOB();
		this.custStatus = customer.getCustStatus();
		this.contractStart = customer.getContractStart();
		this.custBalance = customer.getCustBalance();
	}

	public Customer(String custId, Date creationDate, Timestamp updateDate, String lastName, String firstName,
			Date custDOB, String custStatus, Date contractStart, Integer custBalance) {
		super();
		this.custId = custId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.lastName = lastName;
		this.firstName = firstName;
		this.custDOB = custDOB;
		this.custStatus = custStatus;
		this.contractStart = contractStart;
		this.custBalance = custBalance;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getCustDOB() {
		return custDOB;
	}

	public void setCustDOB(Date custDOB) {
		this.custDOB = custDOB;
	}

	public String getCustStatus() {
		return custStatus;
	}

	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}

	public Date getContractStart() {
		return contractStart;
	}

	public void setContractStart(Date contractStart) {
		this.contractStart = contractStart;
	}

	public Integer getCustBalance() {
		return custBalance;
	}

	public void setCustBalance(Integer custBalance) {
		this.custBalance = custBalance;
	}

	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", creationDate=" + creationDate + ", updateDate=" + updateDate
				+ ", lastName=" + lastName + ", firstName=" + firstName + ", custDOB=" + custDOB + ", custStatus="
				+ custStatus + ", contractStart=" + contractStart + ", custBalance=" + custBalance + "]";
	}

	
	

}
