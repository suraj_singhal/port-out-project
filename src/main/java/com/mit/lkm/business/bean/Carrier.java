package com.mit.lkm.business.bean;

public class Carrier {

	private String carrierId;

	private String carrierName;

	public Carrier() {
		super();
	}
	
	public Carrier(Carrier carrier) {
		this.carrierId = carrier.getCarrierId();
		this.carrierName = carrier.getCarrierName();
	}

	public Carrier(String carrierId, String carrierName) {
		super();
		this.carrierId = carrierId;
		this.carrierName = carrierName;
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	@Override
	public String toString() {
		return "Carrier [carrierId=" + carrierId + ", carrierName=" + carrierName + "]";
	}

	
}
