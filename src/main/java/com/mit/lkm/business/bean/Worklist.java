package com.mit.lkm.business.bean;

import java.sql.Timestamp;
import java.util.Date;

public class Worklist {

	private String taskID;

	private Date creationDate;

	private Timestamp updateDate;

	private String custID;

	private String msisdn;

	private String taskDetails;

	private String taskStatus;

	public Worklist() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Worklist(Worklist worklist) {
		super();

		this.taskID = worklist.getTaskID();
		this.creationDate = worklist.getCreationDate();
		this.updateDate = worklist.getUpdateDate();
		this.custID = worklist.getCustID();
		this.msisdn = worklist.getMsisdn();
		this.taskDetails = worklist.getTaskDetails();
		this.taskStatus = worklist.getTaskStatus();
	}

	public Worklist(String taskID, Date creationDate, Timestamp updateDate, String custID, String msisdn,
			String taskDetails, String taskStatus) {
		super();
		this.taskID = taskID;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.custID = custID;
		this.msisdn = msisdn;
		this.taskDetails = taskDetails;
		this.taskStatus = taskStatus;
	}

	public String getTaskID() {
		return taskID;
	}

	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getCustID() {
		return custID;
	}

	public void setCustID(String custID) {
		this.custID = custID;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getTaskDetails() {
		return taskDetails;
	}

	public void setTaskDetails(String taskDetails) {
		this.taskDetails = taskDetails;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	@Override
	public String toString() {
		return "Worklist [taskID=" + taskID + ", creationDate=" + creationDate + ", updateDate=" + updateDate
				+ ", custID=" + custID + ", msisdn=" + msisdn + ", taskDetails=" + taskDetails + ", taskStatus="
				+ taskStatus + "]";
	}

	
}
