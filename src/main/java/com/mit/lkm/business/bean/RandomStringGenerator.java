package com.mit.lkm.business.bean;

public class RandomStringGenerator {

	public static String getAlphaNumericString(int n) 
    { 
  
        String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            int index 
                = (int)(alphaNumericString.length() 
                        * Math.random()); 
  
            sb.append(alphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    }

	public RandomStringGenerator() {
		super();
	} 
	
}
