package com.mit.lkm.business.bean;

public class ConfirmPortOutProcess {

	private String Back_Order_Id;
	
	private boolean customer_confirmation;
	
	private boolean ready_to_upgrade;
	
	private boolean contract_eligibility;
	
	private boolean confirm_port_out;
	
	private String msisdn;

	public ConfirmPortOutProcess() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfirmPortOutProcess(String back_Order_Id, boolean customer_confirmation, boolean ready_to_upgrade,
			boolean contract_eligibility, boolean confirm_port_out, String msisdn) {
		super();
		Back_Order_Id = back_Order_Id;
		this.customer_confirmation = customer_confirmation;
		this.ready_to_upgrade = ready_to_upgrade;
		this.contract_eligibility = contract_eligibility;
		this.confirm_port_out = confirm_port_out;
		this.msisdn = msisdn;
	}

	public String getBack_Order_Id() {
		return Back_Order_Id;
	}

	public void setBack_Order_Id(String back_Order_Id) {
		Back_Order_Id = back_Order_Id;
	}

	public boolean isCustomer_confirmation() {
		return customer_confirmation;
	}

	public void setCustomer_confirmation(boolean customer_confirmation) {
		this.customer_confirmation = customer_confirmation;
	}

	public boolean isReady_to_upgrade() {
		return ready_to_upgrade;
	}

	public void setReady_to_upgrade(boolean ready_to_upgrade) {
		this.ready_to_upgrade = ready_to_upgrade;
	}

	public boolean isContract_eligibility() {
		return contract_eligibility;
	}

	public void setContract_eligibility(boolean contract_eligibility) {
		this.contract_eligibility = contract_eligibility;
	}

	public boolean isConfirm_port_out() {
		return confirm_port_out;
	}

	public void setConfirm_port_out(boolean confirm_port_out) {
		this.confirm_port_out = confirm_port_out;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@Override
	public String toString() {
		return "ConfirmPortOutProcess [Back_Order_Id=" + Back_Order_Id + ", customer_confirmation="
				+ customer_confirmation + ", ready_to_upgrade=" + ready_to_upgrade + ", contract_eligibility="
				+ contract_eligibility + ", confirm_port_out=" + confirm_port_out + ", msisdn=" + msisdn + "]";
	}
	
	
	
	
}
