package com.mit.lkm.business.bean;

import java.sql.Timestamp;
import java.util.Date;

public class NumberInv {

	private String msisdn;

	private Date creationDate;

	private Timestamp updateDate;

	private String msisdnStatus;

	private String statusReason;

	public NumberInv() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public NumberInv(NumberInv numberInv) {
		super();
		this.msisdn = numberInv.getMsisdn();
		this.creationDate = numberInv.getCreationDate();
		this.updateDate = numberInv.getUpdateDate();
		this.msisdnStatus = numberInv.getMsisdnStatus();
		this.statusReason = numberInv.getStatusReason();
	}

	public NumberInv(String msisdn, Date creationDate, Timestamp updateDate, String msisdnStatus, String statusReason) {
		super();
		this.msisdn = msisdn;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.msisdnStatus = msisdnStatus;
		this.statusReason = statusReason;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getMsisdnStatus() {
		return msisdnStatus;
	}

	public void setMsisdnStatus(String msisdnStatus) {
		this.msisdnStatus = msisdnStatus;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	@Override
	public String toString() {
		return "NumberInv [msisdn=" + msisdn + ", creationDate=" + creationDate + ", updateDate=" + updateDate
				+ ", msisdnStatus=" + msisdnStatus + ", statusReason=" + statusReason + "]";
	}

	
}
