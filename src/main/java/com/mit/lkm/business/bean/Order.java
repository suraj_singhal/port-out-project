package com.mit.lkm.business.bean;

import java.sql.Timestamp;
import java.util.Date;

public class Order {

	private String orderId;

	private Date creationDate;

	private Timestamp updateDate;

	private String custId;

	private String msisdn;

	private String extPortRequest;

	private String carrierId;

	private String orderStatus;

	private String statusReason;

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Order(Order order) {
		super();

		this.orderId = order.getOrderId();
		this.creationDate = order.getCreationDate();
		this.updateDate = order.getUpdateDate();
		this.custId = order.getCustId();
		this.msisdn = order.getMsisdn();
		this.extPortRequest = order.getExtPortRequest();
		this.carrierId = order.getCarrierId();
		this.orderStatus = order.getOrderStatus();
		this.statusReason = order.getStatusReason();
	}

	public Order(String orderId, Date creationDate, Timestamp updateDate, String custId, String msisdn,
			String extPortRequest, String carrierId, String orderStatus, String statusReason) {
		super();
		this.orderId = orderId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.custId = custId;
		this.msisdn = msisdn;
		this.extPortRequest = extPortRequest;
		this.carrierId = carrierId;
		this.orderStatus = orderStatus;
		this.statusReason = statusReason;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getExtPortRequest() {
		return extPortRequest;
	}

	public void setExtPortRequest(String extPortRequest) {
		this.extPortRequest = extPortRequest;
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", creationDate=" + creationDate + ", updateDate=" + updateDate
				+ ", custId=" + custId + ", msisdn=" + msisdn + ", extPortRequest=" + extPortRequest + ", carrierId="
				+ carrierId + ", orderStatus=" + orderStatus + ", statusReason=" + statusReason + "]";
	}

	
}
