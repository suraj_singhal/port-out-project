package com.mit.lkm.business.bean;

import java.util.Date;

public class PortOutRequest {

	private String extPortRequest;
	
	private String msisdn;
	
	private String custFName;
	
	private String custLName;
	
	private Date dob;
	
	private String carrierId;
	
	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public PortOutRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PortOutRequest(PortOutRequest portOutRequest) {
		super();
		this.extPortRequest = portOutRequest.getExtPortRequest();
		this.msisdn = portOutRequest.getMsisdn();
		this.custFName = portOutRequest.getCustFName();
		this.custLName = portOutRequest.getCustLName();
		this.dob = portOutRequest.getDob();
		this.carrierId = portOutRequest.getCarrierId();
	}

	public PortOutRequest(String extPortRequest, String msisdn, String custFName, String custLName, Date dob,
			String carrierId) {
		super();
		this.extPortRequest = extPortRequest;
		this.msisdn = msisdn;
		this.custFName = custFName;
		this.custLName = custLName;
		this.dob = dob;
		this.carrierId = carrierId;
	}

	public String getExtPortRequest() {
		return extPortRequest;
	}

	public void setExtPortRequest(String extPortRequest) {
		this.extPortRequest = extPortRequest;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getCustFName() {
		return custFName;
	}

	public void setCustFName(String custFName) {
		this.custFName = custFName;
	}

	public String getCustLName() {
		return custLName;
	}

	public void setCustLName(String custLName) {
		this.custLName = custLName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "PortOutRequest [extPortRequest=" + extPortRequest + ", msisdn=" + msisdn + ", custFName=" + custFName
				+ ", custLName=" + custLName + ", dob=" + dob + ", carrierId=" + carrierId + "]";
	}

	
	
	
}
