package com.mit.lkm.service;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mit.lkm.business.bean.Carrier;
import com.mit.lkm.business.bean.ConfirmPortOutProcess;
import com.mit.lkm.business.bean.Customer;
import com.mit.lkm.business.bean.NumberInv;
import com.mit.lkm.business.bean.Order;
import com.mit.lkm.business.bean.PortOutRequest;
import com.mit.lkm.business.bean.Worklist;

@Service
public class ValidateRequestServiceImpl {

	@Autowired
	private PortOutRequestServiceImpl portOutRequestServiceImpl;
	
	@Autowired
	private CustomerServiceImpl customerServiceImpl;
	
	@Autowired
	private CarrierServiceImpl carrierServiceImpl;
	
	@Autowired
	private NumberInvServiceImpl numberInvServiceImpl;
	
	@Autowired
	private OrderServiceImpl orderServiceImpl;
	
	@Autowired
	private WorklistServiceImpl worklistServiceImpl;
	
	public String validatePortOutRequest(PortOutRequest portOutRequest) {
		
		if(portOutRequest.getCarrierId()==null || portOutRequest.getCustFName()==null || portOutRequest.getCustLName()==null || portOutRequest.getDob()==null || portOutRequest.getExtPortRequest()==null || portOutRequest.getMsisdn()==null) {
			
			return "Incomplete data in the Port Out Request. Please ensure you provide all information correctly";
		}
		PortOutRequest portOutRequest2 = portOutRequestServiceImpl.getPortOutRequestById(portOutRequest.getExtPortRequest());
		
		if(portOutRequest2!=null) {
			
			return "PortOut request already exists for the given EXTPortOutRequest id";
		}
		
		if(portOutRequest.getExtPortRequest().contains("EXT") && portOutRequest.getExtPortRequest().length()==13) {
		}
		else {
			
			return "Invalid EXTPortOutRequest id";
		}
		
		Carrier carrier = carrierServiceImpl.getCarrierById(portOutRequest.getCarrierId());
		
		if(carrier==null || carrier.getCarrierId().equalsIgnoreCase("ID"))
		{
			
			return "Invalid Carrier id";
		}
		
		if(portOutRequest.getMsisdn().length()!=10 || !StringUtils.isNumeric(portOutRequest.getMsisdn())) {
			return "Please enter correct MSISDN. only 10 digit decimal number allowed"; 
		}
		
		NumberInv numberInv = numberInvServiceImpl.getNumberInvById(portOutRequest.getMsisdn());
		
		if(numberInv==null) {
			
			return "MSISDN not present in the Inventory";
		}
		else if(numberInv.getMsisdnStatus().equalsIgnoreCase("ACTIVE") || numberInv.getMsisdnStatus().equalsIgnoreCase("SUSPEND")) {
			
			if(numberInv.getMsisdnStatus().equalsIgnoreCase("SUSPEND")) {
				if(!(numberInv.getStatusReason().equalsIgnoreCase("INCOLLECTION"))) {
					
					return "SUSPENDED MSISDN with status other than INCOLLECTION is not allowed to be Ported-out";
				}
			}
			
		}
		else {
			
			return "MSISDN with status : " + numberInv.getMsisdnStatus() + " is not allowed to be Ported-out";
		}
		
		
		Collection<Customer> tempCustomerList = customerServiceImpl.getAllCustomer();
		Customer customer = null;
		
		for(Customer customer2 : tempCustomerList) {
			System.out.println("Customer2 : "+customer2);
			if(customer2.getFirstName().equalsIgnoreCase(portOutRequest.getCustFName()) && customer2.getLastName().equalsIgnoreCase(portOutRequest.getCustLName()) && customer2.getCustDOB().getDate()==portOutRequest.getDob().getDate() && customer2.getCustDOB().getMonth()==portOutRequest.getDob().getMonth() && customer2.getCustDOB().getYear()==portOutRequest.getDob().getYear()) {
				customer = new Customer(customer2);
			}
		}
		
		if(customer==null) {
			
			return "Customer with given Name or BOD not found";
		}
		
		if(customer.getCustBalance()>0) {
			
			return "Customer has some dues to be cleared. Cannot Port out before clearing those.";
		}
		
		
		Collection<Order> tempOrderList = orderServiceImpl.getAllOrder();
		Collection<Order> orderList = new ArrayList<Order>();
		Order order2 = null;
		if(tempOrderList.isEmpty()) {
			
			return "Empty Order table";
		}
		
		for(Order order : tempOrderList) {
			
			if(order.getMsisdn().equalsIgnoreCase(portOutRequest.getMsisdn()) && order.getCustId().equalsIgnoreCase(customer.getCustId())) {
				order2 = new Order(order);
				orderList.add(order2);
			}
		}
		
		if(orderList.isEmpty()) {
			
			return "MSISDN and Customer Mapping not found";
		}
		else {
			
			for(Order order : orderList) {
				if(order.getOrderStatus().equalsIgnoreCase("CANCELLED")) {
					
					return "There is a Cancelled Order for the given Customer details. Cannot process Port Out application.";
				}
			}
			
		}
		
		
		return customer.getCustId();
	}
	
	
	public String verifyPortOutProcess(ConfirmPortOutProcess confirmPortOutProcess) {
		
		if(confirmPortOutProcess.getBack_Order_Id()==null || confirmPortOutProcess.getMsisdn()==null) {
			return "Invalid Values";
		}
		
		Worklist worklist = worklistServiceImpl.getWorklistById(confirmPortOutProcess.getBack_Order_Id());
		if(worklist==null) {
			return "Enter Correct BackOrder ID";
		}
		else if(!worklist.getMsisdn().equalsIgnoreCase(confirmPortOutProcess.getMsisdn())) {
			return "Back Order request does not belong to the Mobile no. specified";
		}
		
		
		
		
		return "Valid";
		
	}
}
