package com.mit.lkm.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mit.lkm.business.bean.Customer;
import com.mit.lkm.business.bean.Order;
import com.mit.lkm.business.bean.PortOutRequest;
import com.mit.lkm.business.bean.RandomStringGenerator;
import com.mit.lkm.business.bean.Worklist;
import com.mit.lkm.dao.OrderDao;
import com.mit.lkm.entity.OrderEntity;

@Service
public class OrderServiceImpl {

	
	@Autowired
	private OrderDao orderDao;
	
	
	public String addOrder(Order order) {
		
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setCarrierId(order.getCarrierId());
		orderEntity.setCreationDate(new java.sql.Date(order.getCreationDate().getTime()));
		orderEntity.setCustId(order.getCustId());
		orderEntity.setExtPortRequest(order.getExtPortRequest());
		orderEntity.setMsisdn(order.getMsisdn());
		orderEntity.setOrderId(order.getOrderId());
		orderEntity.setOrderStatus(order.getOrderStatus());
		orderEntity.setUpdateDate(new java.sql.Timestamp(new Date().getTime()));
		orderEntity.setStatusReason(order.getStatusReason());
		OrderEntity orderEntity2 = orderDao.save(orderEntity);
		
		return orderEntity2.getOrderId();
		
	}
	
	
	public String addOrderByDatabase(Customer customer, Worklist worklist, PortOutRequest portOutRequest) {
		
		Order order = new Order();
		order.setCarrierId(portOutRequest.getCarrierId());
		order.setCreationDate(new java.util.Date());
		order.setCustId(customer.getCustId());
		order.setExtPortRequest(portOutRequest.getExtPortRequest());
		order.setMsisdn(worklist.getMsisdn());
		order.setOrderId(RandomStringGenerator.getAlphaNumericString(6));
		order.setOrderStatus("CREATED");
		order.setStatusReason("PORTOUT");
		order.setUpdateDate(new java.sql.Timestamp(new Date().getTime()));
		
		String id = addOrder(order);
		
		return id;
	}
	
	
	public Collection<Order> getAllOrder(){
		
		Collection<OrderEntity> orderEntityList = orderDao.findAll();
		List<Order> orderList = new ArrayList<Order> ();
		
		for(OrderEntity e : orderEntityList) {
			Order order = new Order(e.getOrderId(), e.getCreationDate(), e.getUpdateDate(), e.getCustId(), e.getMsisdn(), e.getExtPortRequest(), e.getCarrierId(), e.getOrderStatus(), e.getStatusReason());
			orderList.add(order);
		}
		
		return orderList;
	}
	
	
	public Order getOrderDetailsByOrderId(String orderId) {
		Order order = null;
		Optional<OrderEntity> orderEntity1 = orderDao.findById(orderId);
		
		if(orderEntity1.isPresent()) {
			OrderEntity orderEntity = orderEntity1.get();
			order = new Order(orderEntity.getOrderId(), orderEntity.getCreationDate(), orderEntity.getUpdateDate(), orderEntity.getCustId(), orderEntity.getMsisdn(), orderEntity.getExtPortRequest(), orderEntity.getCarrierId(), orderEntity.getOrderStatus(), orderEntity.getStatusReason());
		}
		
		return order;
		
	}
	
	public Order deleteOrder(String orderId) {
		Order order = null;
		Optional<OrderEntity> orderEntity1 = orderDao.findById(orderId);
		
		if(orderEntity1.isPresent()) {
			OrderEntity orderEntity = orderEntity1.get();
			orderDao.delete(orderEntity);
			order = new Order(orderEntity.getOrderId(), orderEntity.getCreationDate(), orderEntity.getUpdateDate(), orderEntity.getCustId(), orderEntity.getMsisdn(), orderEntity.getExtPortRequest(), orderEntity.getCarrierId(), orderEntity.getOrderStatus(), orderEntity.getStatusReason());
		}
		
		return order;
	}
	
	public Order updateOrder(Order order) {
		Order tempOrder = null;
		Optional<OrderEntity> orderEntity1 = orderDao.findById(order.getOrderId());
		if(orderEntity1.isPresent()) {
			OrderEntity orderEntity = orderEntity1.get();
			orderEntity.setCarrierId(order.getCarrierId());
			orderEntity.setCreationDate(new java.sql.Date(order.getCreationDate().getTime()));
			orderEntity.setCustId(order.getCustId());
			orderEntity.setExtPortRequest(order.getExtPortRequest());
			orderEntity.setMsisdn(order.getMsisdn());
			orderEntity.setOrderStatus(order.getOrderStatus());
			orderEntity.setUpdateDate(new java.sql.Timestamp(new Date().getTime()));
			orderEntity.setStatusReason(order.getStatusReason());
			orderDao.save(orderEntity);
			
			tempOrder = new Order(orderEntity.getOrderId(), orderEntity.getCreationDate(), orderEntity.getUpdateDate(), orderEntity.getCustId(), orderEntity.getMsisdn(), orderEntity.getExtPortRequest(), orderEntity.getCarrierId(), orderEntity.getOrderStatus(), orderEntity.getStatusReason());
		}
		
		return tempOrder;
	}
	
}
