package com.mit.lkm.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mit.lkm.business.bean.Customer;
import com.mit.lkm.business.bean.NumberInv;
import com.mit.lkm.business.bean.Order;
import com.mit.lkm.business.bean.PortOutRequest;
import com.mit.lkm.dao.PortOutRequestDao;
import com.mit.lkm.entity.PortOutRequestEntity;

@Service
public class PortOutRequestServiceImpl {

	@Autowired
	private PortOutRequestDao portOutRequestDao;
	
	@Autowired
	ValidateRequestServiceImpl validateRequestServiceImpl;
	
	@Autowired
	WorklistServiceImpl worklistServiceImpl;
	
	@Autowired
	CustomerServiceImpl customerServiceImpl;
	
	@Autowired
	OrderServiceImpl orderServiceImpl;
	
	@Autowired
	NumberInvServiceImpl numberInvServiceImpl;
	
	
	public String addPortOutRequest(PortOutRequest portOutRequest) {
		
		PortOutRequestEntity entity = new PortOutRequestEntity(portOutRequest.getExtPortRequest(), portOutRequest.getMsisdn(), portOutRequest.getCustFName(), portOutRequest.getCustLName(), portOutRequest.getCarrierId(), new java.sql.Date(portOutRequest.getDob().getTime()));
		
		PortOutRequestEntity entity2 = portOutRequestDao.save(entity);
		
		return entity2.getExtPortRequest();
	}
	
	
	public Collection<PortOutRequest> getAllPortOutRequest(){
		
		Collection<PortOutRequestEntity> portOutRequestEntityList = portOutRequestDao.findAll();
		
		List<PortOutRequest> portOutRequestList = new ArrayList<PortOutRequest> ();
		
		for(PortOutRequestEntity e : portOutRequestEntityList) {
			PortOutRequest portOutRequest = new PortOutRequest(e.getExtPortRequest(), e.getMsisdn(), e.getFname(), e.getLname(), e.getDob(), e.getCarrierID());
			
			portOutRequestList.add(portOutRequest);
			
		}
		
		return portOutRequestList;
	}
	
	
	public PortOutRequest getPortOutRequestById(String id) {
		PortOutRequest portOutRequest = null;
		Optional<PortOutRequestEntity> entity1  = portOutRequestDao.findById(id);
		
		if(entity1.isPresent()) {
			PortOutRequestEntity entity = entity1.get();
			portOutRequest = new PortOutRequest(entity.getExtPortRequest(), entity.getMsisdn(), entity.getFname(), entity.getLname(), entity.getDob(), entity.getCarrierID());
		}
		return portOutRequest;
	}
	
	public PortOutRequest deletePortOutRequest(String id) {
		PortOutRequest portOutRequest = null;
		Optional<PortOutRequestEntity> entity1  = portOutRequestDao.findById(id);
		
		if(entity1.isPresent()) {
			PortOutRequestEntity entity = entity1.get();
			portOutRequestDao.delete(entity);
			portOutRequest = new PortOutRequest(entity.getExtPortRequest(), entity.getMsisdn(), entity.getFname(), entity.getLname(), entity.getDob(), entity.getCarrierID());
		}
		return portOutRequest;
	}
	
	public String processPortOutOrder(String orderId) {
		
		Order order = orderServiceImpl.getOrderDetailsByOrderId(orderId);
		order.setOrderStatus("SUBMITTED");
		order.setStatusReason("PORTOUT_DONE");
		order.setUpdateDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		
		Order order2 = orderServiceImpl.updateOrder(order);
		
		NumberInv numberInv = numberInvServiceImpl.getNumberInvById(order.getMsisdn());
		numberInv.setMsisdnStatus("DISCONNECT");
		numberInv.setStatusReason("PORTOUT");
		numberInv.setUpdateDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		
		NumberInv numberInv2 = numberInvServiceImpl.updateNumberInv(numberInv);
		
		Customer customer = customerServiceImpl.getCustomerById(order.getCustId());
		customer.setUpdateDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		customer.setCustStatus("CANCELLED");
		
		Customer customer2 = customerServiceImpl.updateCustomer(customer);
		
		if(order2!=null && numberInv2!=null && customer2!=null){
			return "Ok";
		}
		else {
			return "Failure";
		}
	}
}
