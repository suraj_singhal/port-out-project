package com.mit.lkm.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mit.lkm.business.bean.Carrier;
import com.mit.lkm.dao.CarrierDao;
import com.mit.lkm.entity.CarrierEntity;

@Service
public class CarrierServiceImpl {

	
	@Autowired
	private CarrierDao carrierDao;
	
	public Collection<Carrier> getAllCarrier(){
		Collection<CarrierEntity> carrierEntityList = carrierDao.findAll();
		
		List<Carrier> list = new ArrayList<Carrier>();
		
		for(CarrierEntity e : carrierEntityList) {
			Carrier carrier = new Carrier(e.getCarrierId(), e.getCarrierName());
			list.add(carrier);
		}
		
		return list;
	}
	
	public Carrier getCarrierById(String id) {
		Carrier carrier = null;
		
		Optional<CarrierEntity> carrierEntity1 = carrierDao.findById(id);
		
		if(carrierEntity1.isPresent()) {
			CarrierEntity carrierEntity = carrierEntity1.get();
			carrier = new Carrier(carrierEntity.getCarrierId(), carrierEntity.getCarrierName());
		}
		
		return carrier;
	}
}
