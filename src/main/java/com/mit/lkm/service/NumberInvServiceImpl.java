package com.mit.lkm.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mit.lkm.business.bean.NumberInv;
import com.mit.lkm.dao.NumberInvDao;
import com.mit.lkm.entity.NumberInvEntity;

@Service
public class NumberInvServiceImpl {

	@Autowired
	private NumberInvDao numberInvDao;
	
	
	public NumberInv getNumberInvById(String id) {
		
		NumberInv numberInv = null;
		Optional<NumberInvEntity> numberInvEntity1 = numberInvDao.findById(id);
		
		if(numberInvEntity1.isPresent()) {
			NumberInvEntity numberInvEntity = numberInvEntity1.get();
			numberInv = new NumberInv(numberInvEntity.getMsisdn(), numberInvEntity.getCreationDate(), numberInvEntity.getUpdateDate(), numberInvEntity.getMsisdnStatus(), numberInvEntity.getStatusReason());
		}
		
		return numberInv;
		
	}
	
	public Collection<NumberInv> getAllNumberInv(){
		
		Collection<NumberInvEntity> numberInvEntityList = numberInvDao.findAll();
		
		List<NumberInv> list = new ArrayList<NumberInv> ();
		
		for(NumberInvEntity numberInvEntity : numberInvEntityList) {
			NumberInv numberInv = new NumberInv(numberInvEntity.getMsisdn(), numberInvEntity.getCreationDate(), numberInvEntity.getUpdateDate(), numberInvEntity.getMsisdnStatus(), numberInvEntity.getStatusReason());
			
			list.add(numberInv);
			
		}
		
		return list;
		
	}
	

	public NumberInv deleteNumberInv(String id) {
		
		NumberInv numberInv = null;
		Optional<NumberInvEntity> numberInvEntity1 = numberInvDao.findById(id);
		
		if(numberInvEntity1.isPresent()) {
			NumberInvEntity numberInvEntity = numberInvEntity1.get();
			numberInvDao.delete(numberInvEntity);
			numberInv = new NumberInv(numberInvEntity.getMsisdn(), numberInvEntity.getCreationDate(), numberInvEntity.getUpdateDate(), numberInvEntity.getMsisdnStatus(), numberInvEntity.getStatusReason());
		}
		
		return numberInv;	
	}
	
	public NumberInv updateNumberInv(NumberInv numberInv) {
		NumberInv tempNumberInv = null;
		Optional<NumberInvEntity> numberInvEntity1 = numberInvDao.findById(numberInv.getMsisdn());
		if(numberInvEntity1.isPresent()) {
			NumberInvEntity numberInvEntity = numberInvEntity1.get();
			numberInvEntity.setCreationDate(new java.sql.Date(numberInv.getCreationDate().getTime()));
			numberInvEntity.setMsisdnStatus(numberInv.getMsisdnStatus());
			numberInvEntity.setUpdateDate(new java.sql.Timestamp(new java.util.Date().getTime()));
			numberInvEntity.setStatusReason(numberInv.getStatusReason());
			
			numberInvDao.save(numberInvEntity);
			
			tempNumberInv = new NumberInv(numberInvEntity.getMsisdn(), numberInvEntity.getCreationDate(), numberInvEntity.getUpdateDate(), numberInvEntity.getMsisdnStatus(), numberInvEntity.getStatusReason());
		}
		
		return tempNumberInv;
	}
}
