package com.mit.lkm.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mit.lkm.business.bean.Customer;
import com.mit.lkm.dao.CustomerDao;
import com.mit.lkm.entity.CustomerEntity;

@Service
public class CustomerServiceImpl {

	
	@Autowired
	private CustomerDao customerDao;
	
	
	public Collection<Customer> getAllCustomer(){
		
		Collection<CustomerEntity> customerEntityList = customerDao.findAll();
		
		List<Customer> list = new ArrayList<Customer>();
		
		for(CustomerEntity customerEntity : customerEntityList) {
			
			Customer customer = new Customer(customerEntity.getCustId(), customerEntity.getCreationDate(), customerEntity.getUpdateDate(), customerEntity.getLastName(), customerEntity.getFirstName(), customerEntity.getCustDOB(), customerEntity.getCustStatus(), customerEntity.getContractStart(), customerEntity.getCustBalance());
			
			list.add(customer);
		}
		
		return list;
	}
	
	public Customer getCustomerById(String id) {
		
		Customer customer = null;
		
		Optional<CustomerEntity> customerEntity1 = customerDao.findById(id);
		
		if(customerEntity1.isPresent()) {
			CustomerEntity customerEntity = customerEntity1.get();
			customer = new Customer(customerEntity.getCustId(), customerEntity.getCreationDate(), customerEntity.getUpdateDate(), customerEntity.getLastName(), customerEntity.getFirstName(), customerEntity.getCustDOB(), customerEntity.getCustStatus(), customerEntity.getContractStart(), customerEntity.getCustBalance());
		
		}
		
		return customer;
	}
	

	public Customer deleteCustomer(String id) {
		
		Customer customer = null;
		
		Optional<CustomerEntity> customerEntity1 = customerDao.findById(id);
		
		if(customerEntity1.isPresent()) {
			CustomerEntity customerEntity = customerEntity1.get();
			customerDao.delete(customerEntity);
			customer = new Customer(customerEntity.getCustId(), customerEntity.getCreationDate(), customerEntity.getUpdateDate(), customerEntity.getLastName(), customerEntity.getFirstName(), customerEntity.getCustDOB(), customerEntity.getCustStatus(), customerEntity.getContractStart(), customerEntity.getCustBalance());
		
		}
		
		return customer;
	}
	
	public Customer updateCustomer(Customer customer) {
		Customer tempCustomer = null;
		
		Optional<CustomerEntity> customerEntity1 = customerDao.findById(customer.getCustId());
		
		if(customerEntity1.isPresent()) {
			CustomerEntity customerEntity = customerEntity1.get();
			customerEntity.setContractStart(new java.sql.Date(customer.getContractStart().getTime()));
			customerEntity.setCreationDate(new java.sql.Date(customer.getCreationDate().getTime()));
			customerEntity.setCustBalance(customer.getCustBalance());
			customerEntity.setCustDOB(new java.sql.Date(customer.getCustDOB().getTime()));
			customerEntity.setCustStatus(customer.getCustStatus());
			customerEntity.setFirstName(customer.getFirstName());
			customerEntity.setLastName(customer.getLastName());
			customerEntity.setUpdateDate(new java.sql.Timestamp(new java.util.Date().getTime()));
			
			customerDao.save(customerEntity);
			
			tempCustomer = new Customer(customerEntity.getCustId(), customerEntity.getCreationDate(), customerEntity.getUpdateDate(), customerEntity.getLastName(), customerEntity.getFirstName(), customerEntity.getCustDOB(), customerEntity.getCustStatus(), customerEntity.getContractStart(), customerEntity.getCustBalance());
			
		}
		return tempCustomer;
	}
	
	public String addCustomer(Customer customer) {
		
		CustomerEntity customerEntity = new CustomerEntity();
		customerEntity.setCustId(customer.getCustId());
		customerEntity.setContractStart(new java.sql.Date(customer.getContractStart().getTime()));
		customerEntity.setCreationDate(new java.sql.Date(customer.getCreationDate().getTime()));
		customerEntity.setCustBalance(customer.getCustBalance());
		customerEntity.setCustDOB(new java.sql.Date(customer.getCustDOB().getTime()));
		customerEntity.setCustStatus(customer.getCustStatus());
		customerEntity.setFirstName(customer.getFirstName());
		customerEntity.setLastName(customer.getLastName());
		customerEntity.setUpdateDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		
		CustomerEntity customerEntity2 = customerDao.save(customerEntity);
		
		return customerEntity2.getCustId();
	}
	
}
