package com.mit.lkm.service;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mit.lkm.business.bean.Worklist;
import com.mit.lkm.dao.WorklistDao;
import com.mit.lkm.entity.WorklistEntity;

@Service
public class WorklistServiceImpl {

	
	@Autowired
	private WorklistDao worklistDao;
	
	
	public String addWorklist(Worklist worklist) {
		
		WorklistEntity worklistEntity = new WorklistEntity();
		
		worklistEntity.setCreationDate(new java.sql.Date(worklist.getCreationDate().getTime()));
		worklistEntity.setCustID(worklist.getCustID());
		worklistEntity.setMsisdn(worklist.getMsisdn());
		worklistEntity.setTaskDetails(worklist.getTaskDetails());
		worklistEntity.setTaskID(worklist.getTaskID());
		worklistEntity.setTaskStatus(worklist.getTaskStatus());
		worklistEntity.setUpdateDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		
		WorklistEntity worklistEntity2 = worklistDao.save(worklistEntity);
		
		return worklistEntity2.getTaskID();
	}
	
	public Worklist getWorklistById(String id) {
		
		Worklist worklist = null;
		
		Optional<WorklistEntity> worklistEntity1 = worklistDao.findById(id);
		
		if(worklistEntity1.isPresent()) {
			WorklistEntity worklistEntity = worklistEntity1.get();
			worklist = new Worklist(worklistEntity.getTaskID(), worklistEntity.getCreationDate(), worklistEntity.getUpdateDate(), worklistEntity.getCustID(), worklistEntity.getMsisdn(), worklistEntity.getTaskDetails(), worklistEntity.getTaskStatus());
			
		}
		
		return worklist;
	}
	

	public Worklist deleteWorklist(String id) {
		
		Worklist worklist = null;
		
		Optional<WorklistEntity> worklistEntity1 = worklistDao.findById(id);
		
		if(worklistEntity1.isPresent()) {
			WorklistEntity worklistEntity = worklistEntity1.get();
			worklistDao.delete(worklistEntity);
			worklist = new Worklist(worklistEntity.getTaskID(), worklistEntity.getCreationDate(), worklistEntity.getUpdateDate(), worklistEntity.getCustID(), worklistEntity.getMsisdn(), worklistEntity.getTaskDetails(), worklistEntity.getTaskStatus());
			
		}
		
		return worklist;
	}
	
	public Collection<Worklist> getAllWorklist(){
		
		Collection<WorklistEntity> worklistEntityList = worklistDao.findAll();
		
		List<Worklist> list = new ArrayList<Worklist>();
		
		for(WorklistEntity worklistEntity : worklistEntityList) {
			
			Worklist worklist = new Worklist(worklistEntity.getTaskID(), worklistEntity.getCreationDate(), worklistEntity.getUpdateDate(), worklistEntity.getCustID(), worklistEntity.getMsisdn(), worklistEntity.getTaskDetails(), worklistEntity.getTaskStatus());
			
			list.add(worklist);
		}
		
		return list;
	}
	
	public Worklist updateWorkList(Worklist worklist) {
		
		Worklist tempWorklist = null;
		
		Optional<WorklistEntity> worklistEntity1 = worklistDao.findById(worklist.getTaskID());
		
		if(worklistEntity1.isPresent()) {
			WorklistEntity worklistEntity = worklistEntity1.get();
			worklistEntity.setCreationDate(new java.sql.Date(worklist.getCreationDate().getTime()));
			worklistEntity.setCustID(worklist.getCustID());
			worklistEntity.setMsisdn(worklist.getMsisdn());
			worklistEntity.setTaskDetails(worklist.getTaskDetails());
			worklistEntity.setTaskStatus(worklist.getTaskStatus());
			worklistEntity.setUpdateDate(new java.sql.Timestamp(new java.util.Date().getTime()));
			
			worklistDao.save(worklistEntity);
			
			tempWorklist = new Worklist(worklistEntity.getTaskID(), worklistEntity.getCreationDate(), worklistEntity.getUpdateDate(), worklistEntity.getCustID(), worklistEntity.getMsisdn(), worklistEntity.getTaskDetails(), worklistEntity.getTaskStatus());
			
		}
		return tempWorklist;
	}
}
