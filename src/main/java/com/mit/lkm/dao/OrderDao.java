package com.mit.lkm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mit.lkm.entity.OrderEntity;

public interface OrderDao extends JpaRepository<OrderEntity, String>{

}
