package com.mit.lkm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mit.lkm.entity.NumberInvEntity;

public interface NumberInvDao extends JpaRepository<NumberInvEntity, String>{

}
