package com.mit.lkm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mit.lkm.entity.CustomerEntity;

public interface CustomerDao extends JpaRepository<CustomerEntity, String>{

}
