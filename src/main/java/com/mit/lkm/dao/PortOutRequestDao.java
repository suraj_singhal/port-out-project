package com.mit.lkm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mit.lkm.entity.PortOutRequestEntity;

public interface PortOutRequestDao extends JpaRepository<PortOutRequestEntity, String>{

}
