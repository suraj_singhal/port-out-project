package com.mit.lkm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mit.lkm.entity.WorklistEntity;

public interface WorklistDao extends JpaRepository<WorklistEntity, String>{

}
