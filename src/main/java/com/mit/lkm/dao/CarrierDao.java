package com.mit.lkm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mit.lkm.entity.CarrierEntity;

public interface CarrierDao extends JpaRepository<CarrierEntity, String>{

}
