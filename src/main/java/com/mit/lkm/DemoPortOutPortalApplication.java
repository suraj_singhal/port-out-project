package com.mit.lkm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPortOutPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPortOutPortalApplication.class, args);
	}

}
