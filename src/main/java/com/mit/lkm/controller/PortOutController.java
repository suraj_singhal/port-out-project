package com.mit.lkm.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mit.lkm.business.bean.ConfirmPortOutProcess;
import com.mit.lkm.business.bean.Customer;
import com.mit.lkm.business.bean.PortOutRequest;
import com.mit.lkm.business.bean.RandomStringGenerator;
import com.mit.lkm.business.bean.Worklist;
import com.mit.lkm.service.CustomerServiceImpl;
import com.mit.lkm.service.OrderServiceImpl;
import com.mit.lkm.service.PortOutRequestServiceImpl;
import com.mit.lkm.service.ValidateRequestServiceImpl;
import com.mit.lkm.service.WorklistServiceImpl;

@RestController
@RequestMapping("/CarrierService")
public class PortOutController {


	
	@Autowired
	PortOutRequestServiceImpl portOutRequestServiceImpl;
	
	@Autowired
	ValidateRequestServiceImpl validateRequestServiceImpl;
	
	@Autowired
	WorklistServiceImpl worklistServiceImpl;
	
	@Autowired
	CustomerServiceImpl customerServiceImpl;
	
	@Autowired
	OrderServiceImpl orderServiceImpl;
	
	@RequestMapping(value = "/createPortOutRequest", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.TEXT_HTML_VALUE)
	public ResponseEntity<String> addPortOutRequest(@RequestBody PortOutRequest portOutRequest){
		System.out.println("PortOut request received from user : "+portOutRequest);
		String validity = validateRequestServiceImpl.validatePortOutRequest(portOutRequest);
		if(validity.length()==10 && validity.contains("CU00")) {
		String portOutRequestId = portOutRequestServiceImpl.addPortOutRequest(portOutRequest);
		Worklist worklist = new Worklist(RandomStringGenerator.getAlphaNumericString(12), new java.util.Date(), new java.sql.Timestamp( new java.util.Date().getTime()), validity, portOutRequest.getMsisdn(), "PortOut Request", "CREATED");
		String worklistId = worklistServiceImpl.addWorklist(worklist);
		
		return new ResponseEntity<String>("Port-Out Request is successfully received with extPortRequest Id:"+ portOutRequestId + ". <br> Also BackOrder system has been updated with the request. Please note the BackOrder Id for your request : " + worklistId , HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<String>(validity , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getAllPortOutRequest", method = RequestMethod.GET, consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<PortOutRequest>> getAllPortOutRequest(){
		
		Collection<PortOutRequest> list = portOutRequestServiceImpl.getAllPortOutRequest();
		return new ResponseEntity<Collection<PortOutRequest>>(list , HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value="/getBackOrderDetail/{id}",method=RequestMethod.GET,produces=MediaType.TEXT_HTML_VALUE)
	public ResponseEntity<String> getBackOrderDetail(@PathVariable("id") String id){
		Worklist worklist = null;
		worklist = worklistServiceImpl.getWorklistById(id);
		
		if(worklist==null) {
			return new ResponseEntity<String>("No Back Order system entry against the specified ID. PLease enter correct ID." , HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		Customer customer = customerServiceImpl.getCustomerById(worklist.getCustID());
		
		return new ResponseEntity<String>("Below are the details for the Back Order.<br>" +
											"<br>Request Type  : Port Out"+
											"<br>BackOrder ID  : " + worklist.getTaskID() +
											"<br>Customer Name : " + customer.getFirstName() + " " + customer.getLastName() +
											"<br>Mobile Number : " + worklist.getMsisdn() +
											"<br>Request Created on : " + worklist.getCreationDate() +
											"<br>Contract Start Date : " + customer.getContractStart() +
											"<br>Contract Eligibility Period: 1 Year" , HttpStatus.CREATED);
		
	}
	
	
	@RequestMapping(value = "/processPortOutOrder", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.TEXT_HTML_VALUE)	
	public ResponseEntity<String> processPortOutOrder(@RequestBody ConfirmPortOutProcess confirmPortOutProcess){
		System.out.println("Conifrmation returned for Port Out Process : "+ confirmPortOutProcess);
		String validity = validateRequestServiceImpl.verifyPortOutProcess(confirmPortOutProcess);
		
		if(!validity.equalsIgnoreCase("Valid")) {
			return new ResponseEntity<String>(validity , HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(confirmPortOutProcess.isConfirm_port_out()) {
			Worklist worklist = worklistServiceImpl.getWorklistById(confirmPortOutProcess.getBack_Order_Id());
			Customer customer = customerServiceImpl.getCustomerById(worklist.getCustID());
			Collection<PortOutRequest> list = portOutRequestServiceImpl.getAllPortOutRequest();
			PortOutRequest portOutRequest = null;
			
			for(PortOutRequest request : list) {
				if(request.getCustFName().equalsIgnoreCase(customer.getFirstName()) && request.getCustLName().equalsIgnoreCase(customer.getLastName())) {
					portOutRequest = new PortOutRequest(request);
				}
			}
			
			String orderId = orderServiceImpl.addOrderByDatabase(customer, worklist, portOutRequest);
			
			String operation = portOutRequestServiceImpl.processPortOutOrder(orderId);
			if(operation.equalsIgnoreCase("Ok")) {
				return new ResponseEntity<String>("Port Out order has been Created with id : " + orderId + "<br>" +
												"<br>Port Out order has been Submitted/Processed successfully!", HttpStatus.CREATED);
			}
			else {
				return new ResponseEntity<String>("Some error has occured while updating database" , HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		else if(confirmPortOutProcess.isConfirm_port_out()==false) {
			Worklist worklist = worklistServiceImpl.getWorklistById(confirmPortOutProcess.getBack_Order_Id());
			
			worklistServiceImpl.deleteWorklist(worklist.getTaskID());
			Customer customer = customerServiceImpl.getCustomerById(worklist.getCustID());
			Collection<PortOutRequest> list = portOutRequestServiceImpl.getAllPortOutRequest();
			
			for(PortOutRequest request : list) {
				if(request.getCustFName().equalsIgnoreCase(customer.getFirstName()) && request.getCustLName().equalsIgnoreCase(customer.getLastName())) {
					portOutRequestServiceImpl.deletePortOutRequest(request.getExtPortRequest());
				}
			}
			
			return new ResponseEntity<String>("Back Ordered Port out request has been deleted" , HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<String>("Some error has occured in the system" , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@RequestMapping(value = "/getportprocess", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConfirmPortOutProcess> getPortOutProcess(){
		
		ConfirmPortOutProcess list = new ConfirmPortOutProcess("sdg4t", true, false, true, false,"124567");
		
		return new ResponseEntity<ConfirmPortOutProcess>(list , HttpStatus.CREATED);
	}
}
