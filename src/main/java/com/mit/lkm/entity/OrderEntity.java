package com.mit.lkm.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ordertable")
public class OrderEntity {
	
	
	
	@Id
	@Column(name = "orderid")
	private String orderId;
	
	@Column(name = "creationdate")
	private Date creationDate;
	
	@Column(name = "updatedate")
	private Timestamp updateDate;

	@Column(name = "custid")
	private String custId;
	
	@Column(name = "msisdn")
	private String msisdn;
	
	@Column(name = "extportrequest")
	private String extPortRequest;
	
	@Column(name = "carrierid")
	private String carrierId;
	
	@Column(name = "orderstatus")
	private String orderStatus;
	
	@Column(name = "statusreason")
	private String statusReason;

	public OrderEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrderEntity(String orderId, Date creationDate, Timestamp updateDate, String custId, String msisdn,
			String extPortRequest, String carrierId, String orderStatus, String statusReason) {
		super();
		this.orderId = orderId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.custId = custId;
		this.msisdn = msisdn;
		this.extPortRequest = extPortRequest;
		this.carrierId = carrierId;
		this.orderStatus = orderStatus;
		this.statusReason = statusReason;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getExtPortRequest() {
		return extPortRequest;
	}

	public void setExtPortRequest(String extPortRequest) {
		this.extPortRequest = extPortRequest;
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	@Override
	public String toString() {
		return "OrderEntity [orderId=" + orderId + ", creationDate=" + creationDate + ", updateDate=" + updateDate
				+ ", custId=" + custId + ", msisdn=" + msisdn + ", extPortRequest=" + extPortRequest + ", carrierId="
				+ carrierId + ", orderStatus=" + orderStatus + ", statusReason=" + statusReason + "]";
	}
	
	
	
	
	
}
