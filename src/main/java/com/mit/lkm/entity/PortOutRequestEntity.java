package com.mit.lkm.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Portoutrequest")
public class PortOutRequestEntity {

	@Id
	@Column(name = "extportrequest")
	private String extPortRequest;
	
	@Column(name = "msisdn")
	private String msisdn;
	
	@Column(name = "fname")
	private String fname;
	
	@Column(name = "lname")
	private String lname;
	
	@Column(name= "carrierid")
	private String carrierID;
	
	@Column(name= "dob")
	private Date dob;

	public PortOutRequestEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PortOutRequestEntity(PortOutRequestEntity portOutRequestEntity) {

		this.extPortRequest = portOutRequestEntity.getExtPortRequest();
		this.msisdn = portOutRequestEntity.getMsisdn();
		this.fname = portOutRequestEntity.getFname();
		this.lname = portOutRequestEntity.getLname();
		this.carrierID = portOutRequestEntity.getCarrierID();
		this.dob = portOutRequestEntity.getDob();
		
	}

	public PortOutRequestEntity(String extPortRequest, String msisdn, String fname, String lname, String carrierID,
			Date dob) {
		super();
		this.extPortRequest = extPortRequest;
		this.msisdn = msisdn;
		this.fname = fname;
		this.lname = lname;
		this.carrierID = carrierID;
		this.dob = dob;
	}

	public String getExtPortRequest() {
		return extPortRequest;
	}

	public void setExtPortRequest(String extPortRequest) {
		this.extPortRequest = extPortRequest;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getCarrierID() {
		return carrierID;
	}

	public void setCarrierID(String carrierID) {
		this.carrierID = carrierID;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "PortOutRequestEntity [extPortRequest=" + extPortRequest + ", msisdn=" + msisdn + ", fname=" + fname
				+ ", lname=" + lname + ", carrierID=" + carrierID + ", dob=" + dob + "]";
	}
	
	
	
}
