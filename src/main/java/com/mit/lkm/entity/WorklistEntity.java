package com.mit.lkm.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "worklist")
public class WorklistEntity {

	@Id
	@Column(name = "taskid")
	private String taskID;
	
	@Column(name = "creationdate")
	private Date creationDate;
	
	@Column(name = "updatedate")
	private Timestamp updateDate;
	
	@Column(name = "custid")
	private String custID;
	
	@Column(name = "msisdn")
	private String msisdn;
	
	@Column(name = "taskdetails")
	private String taskDetails;
	
	@Column(name = "taskstatus")
	private String taskStatus;

	public WorklistEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WorklistEntity(String taskID, Date creationDate, Timestamp updateDate, String custID, String msisdn,
			String taskDetails, String taskStatus) {
		super();
		this.taskID = taskID;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.custID = custID;
		this.msisdn = msisdn;
		this.taskDetails = taskDetails;
		this.taskStatus = taskStatus;
	}

	public String getTaskID() {
		return taskID;
	}

	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getCustID() {
		return custID;
	}

	public void setCustID(String custID) {
		this.custID = custID;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getTaskDetails() {
		return taskDetails;
	}

	public void setTaskDetails(String taskDetails) {
		this.taskDetails = taskDetails;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	@Override
	public String toString() {
		return "WorklistEntity [taskID=" + taskID + ", creationDate=" + creationDate + ", updateDate=" + updateDate
				+ ", custID=" + custID + ", msisdn=" + msisdn + ", taskDetails=" + taskDetails + ", taskStatus="
				+ taskStatus + "]";
	}
	
	
	
	
	
	
}
