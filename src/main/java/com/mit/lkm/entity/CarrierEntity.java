package com.mit.lkm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "carrier")
public class CarrierEntity {

	@Id
	@Column(name = "carrierid")
	private String carrierId;
	
	@Column(name = "carriername")
	private String carrierName;

	public CarrierEntity(String carrierId, String carrierName) {
		super();
		this.carrierId = carrierId;
		this.carrierName = carrierName;
	}

	public CarrierEntity() {
		super();
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	@Override
	public String toString() {
		return "CarrierEntity [carrierId=" + carrierId + ", carrierName=" + carrierName + "]";
	}
	
	
	
}
