package com.mit.lkm.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "numberinv")
public class NumberInvEntity {

	@Id
	@Column(name = "msisdn")
	private String msisdn;
	
	@Column(name = "creationdate")
	private Date creationDate;
	
	@Column(name = "updatedate")
	private Timestamp updateDate;

	@Column(name = "msisdnstatus")
	private String msisdnStatus;
	
	@Column(name = "statusreason")
	private String statusReason;

	public NumberInvEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NumberInvEntity(String msisdn, Date creationDate, Timestamp updateDate, String msisdnStatus,
			String statusReason) {
		super();
		this.msisdn = msisdn;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.msisdnStatus = msisdnStatus;
		this.statusReason = statusReason;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getMsisdnStatus() {
		return msisdnStatus;
	}

	public void setMsisdnStatus(String msisdnStatus) {
		this.msisdnStatus = msisdnStatus;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	@Override
	public String toString() {
		return "NumberInvEntity [msisdn=" + msisdn + ", creationDate=" + creationDate + ", updateDate=" + updateDate
				+ ", msisdnStatus=" + msisdnStatus + ", statusReason=" + statusReason + "]";
	}
	
	




}
