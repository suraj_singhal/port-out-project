package com.mit.lkm.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class CustomerEntity {

	@Id
	@Column(name = "custid")
	private String custId;
	
	@Column(name = "creationdate")
	private Date creationDate;
	
	@Column(name = "updatedate")
	private Timestamp updateDate;
	
	@Column(name = "lastname")
	private String lastName;
	
	@Column(name = "firstname")
	private String firstName;
	
	@Column(name = "custdob")
	private Date custDOB;
	
	@Column(name = "custstatus")
	private String custStatus;
	
	@Column(name = "contractstart")
	private Date contractStart;
	
	@Column(name = "custbalance")
	private Integer custBalance;

	public CustomerEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomerEntity(String custId, Date creationDate, Timestamp updateDate, String lastName, String firstName,
			Date custDOB, String custStatus, Date contractStart, Integer custBalance) {
		super();
		this.custId = custId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.lastName = lastName;
		this.firstName = firstName;
		this.custDOB = custDOB;
		this.custStatus = custStatus;
		this.contractStart = contractStart;
		this.custBalance = custBalance;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getCustDOB() {
		return custDOB;
	}

	public void setCustDOB(Date custDOB) {
		this.custDOB = custDOB;
	}

	public String getCustStatus() {
		return custStatus;
	}

	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}

	public Date getContractStart() {
		return contractStart;
	}

	public void setContractStart(Date contractStart) {
		this.contractStart = contractStart;
	}

	public Integer getCustBalance() {
		return custBalance;
	}

	public void setCustBalance(Integer custBalance) {
		this.custBalance = custBalance;
	}

	@Override
	public String toString() {
		return "CustomerEntity [custId=" + custId + ", creationDate=" + creationDate + ", updateDate=" + updateDate
				+ ", lastName=" + lastName + ", firstName=" + firstName + ", custDOB=" + custDOB + ", custStatus="
				+ custStatus + ", contractStart=" + contractStart + ", custBalance=" + custBalance + "]";
	}
	
	
	
}
