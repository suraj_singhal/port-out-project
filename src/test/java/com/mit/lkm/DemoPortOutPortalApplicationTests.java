package com.mit.lkm;

import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mit.lkm.business.bean.PortOutRequest;
import com.mit.lkm.controller.PortOutController;
import com.mit.lkm.service.PortOutRequestServiceImpl;

import junit.framework.Assert;



@RunWith(SpringJUnit4ClassRunner.class)

@SpringBootTest(classes = DemoPortOutPortalApplication.class)
class DemoPortOutPortalApplicationTests {

//	@Test
//	void contextLoads() {
//	}
	
	@Mock
	PortOutRequestServiceImpl portOutRequestServiceImpl;
	
	
	@InjectMocks
	PortOutController portOutController;
	
	protected MockMvc mockMvc;
	
	@Before
    public void mySetup(){
    	MockitoAnnotations.initMocks(this);
    	
    	//Step2: Using Use MockMvcBuilders to create a MockMvc which is replica just of Controller 
    	mockMvc = MockMvcBuilders.standaloneSetup(portOutController).build();
	}
	
	@Test
	public void testPortOutRequest() throws Exception{
		
		String uri="/createPortOutRequest";
		Date date = new Date(1993, 07, 21);
		PortOutRequest portOutRequest = new PortOutRequest("EXT0123456789", "9822852124", "Suraj", "Singhal", date, "VF");
		ObjectMapper mapper = new ObjectMapper();
		String portOutRequestJSON = mapper.writeValueAsString(portOutRequest);
		
		MockHttpServletRequestBuilder request= MockMvcRequestBuilders.post(uri)
  			  .accept(MediaType.TEXT_HTML) 
  			  .content(portOutRequestJSON) 
  			  .contentType(MediaType.APPLICATION_JSON);
		
		when(portOutRequestServiceImpl.addPortOutRequest(portOutRequest)).thenReturn("1001");
		
		ResultActions resultActions= mockMvc.perform(request);
		
		MvcResult result = resultActions.andReturn();
		
		String finalResult = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		
		Assert.assertTrue(finalResult!=null);
		Assert.assertTrue(finalResult.contains("1001"));
		Assert.assertTrue(status==HttpStatus.CREATED.value());
		
		
	}

}
